#!/usr/bin/python3

import math
import os
import time
from random import random

import typer
from pynput.mouse import Button, Controller, Events


def random_point(zone):
    one, two = zone[0], zone[1]
    diff_x = abs(one[0] - two[0])
    diff_y = abs(one[1] - two[1])
    r_x = random()
    r_y = random()

    delta_x = diff_x * r_x
    delta_y = diff_y * r_y

    return one[0] + delta_x, one[1] + delta_y


def center(zone):
    one, two = zone[0], zone[1]
    diff_x = abs(one[0] - two[0])
    diff_y = abs(one[1] - two[1])

    return one[0] + diff_x // 2, one[1] + diff_y // 2


def normalize(vector):
    mag = math.sqrt(pow(vector[0], 2) + pow(vector[1], 2))

    return vector[0] / mag, vector[1] / mag


def calculate_direction_vector(center, point):
    direction = (center[0] - point[0], center[1] - point[1])
    return normalize(direction)


def fire(mouse, vec):
    mouse.press(Button.left)
    mouse.move(-vec[0] * 500, -vec[1] * 500)
    mouse.release(Button.left)


def trigger_confetti(mouse, click_zone, point):
    mouse.position = point

    dead_center = center(click_zone)
    dir_vec = calculate_direction_vector(dead_center, point)

    fire(mouse, dir_vec)


def main(click_per_second: int = 100, sleep_delay: float = 0.1):
    """
    Tutorial

        1. Select the confetti tool in metroretro.

        2. Run the script.

        3. Define the blastzone:

            First click should be the upper left corner.

            Second one click should be in the bottom left corner.

        4. Enjoy
    """
    # TODO: Stop execution when pressing ESC key.
    click_zone = []
    mouse = Controller()

    with Events() as events:
        for event in events:
            if isinstance(event, Events.Click) and not event.pressed:
                click_zone.append((event.x, event.y))
                if len(click_zone) >= 2:
                    break

    for i in range(click_per_second):
        point = random_point(click_zone)
        trigger_confetti(mouse, click_zone, point)
        time.sleep(sleep_delay)


if __name__ == '__main__':
    typer.run(main)
