# Tutorial
- Select the confetti tool in metroretro
- run the script
- define the blastzone:
	- first click should be the upper left corner
	- second one should be the bottom left one
- enjoy

# Help
>This script works with the arrow tool too.

- CLICK
	- change how many time you'll blast confetti.
- SLEEP\_MS
	- change how much we wait between confetti blasts.

# Todo
- Stop execution if ESC key is pressed
- Makefile to automate virtual env creation + execution
